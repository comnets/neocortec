#include <sys/socket.h>
#include <linux/types.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <string>
#include <iostream>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

std::string create_message(uint16_t nodeId, std::string str)
{
	std::vector<char> data;
	std::copy(str.c_str(), str.c_str()+str.length(), back_inserter(data));
	json j = {
	  {"objectType", "sendPayload"},
	  {"payloadType", "acknowledged"},
	  {"nodeId", nodeId},
	  {"port", 0},
	  {"payload", data}
	};

	return j.dump();
}
std::string extract_payload(std::string msg)
{
	auto j = json::parse(msg);
	auto v = j.at("payload").get<std::vector<char> >();
	return std::string (v.begin(),v.end());
}
uint16_t extract_senderId(std::string msg)
{
	auto j = json::parse(msg);
	return j["nodeId"];
}

static int main_cl(int argc, char **argv) {
	int sk, port, ret;
	struct sockaddr_in addr;

	sk = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sk < 0) {
		perror("Can't create socket");
		return -1;
	}

	port = atoi(argv[2]);
	printf("Connecting to %s:%d\n", argv[1], port);
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	ret = inet_aton(argv[1], &addr.sin_addr);
	if (ret < 0) {
		perror("Can't convert addr");
		return -1;
	}
	addr.sin_port = htons(port);

	ret = connect(sk, (struct sockaddr *) &addr, sizeof(addr));
	if (ret < 0) {
		perror("Can't connect");
		return -1;
	}

	std::string msg = create_message(0x0030, "say_HELLO!");

	write(sk, msg.c_str(), msg.size());
	std::cout << msg << std::endl;

	return 0;

}

static int main_cl_rcv(int argc, char **argv) {
	int sk, port, ret;
	struct sockaddr_in addr;

	sk = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sk < 0) {
		perror("Can't create socket");
		return -1;
	}

	port = atoi(argv[2]);
	printf("Connecting to %s:%d\n", argv[1], port);
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	ret = inet_aton(argv[1], &addr.sin_addr);
	if (ret < 0) {
		perror("Can't convert addr");
		return -1;
	}
	addr.sin_port = htons(port);

	ret = connect(sk, (struct sockaddr *) &addr, sizeof(addr));
	if (ret < 0) {
		perror("Can't connect");
		return -1;
	}

	int rd;
	char buf[1024];

	printf("New connection\n");

	while (1) {
		rd = read(sk, buf, sizeof(buf));
		printf("READ\n");
		if (!rd)
			break;

		if (rd < 0) {
			perror("Can't read socket");
			return 1;
		}
		std::string rmsg(&buf[0], rd);
		std::cout << rmsg << std::endl;
		std::cout << "Payload: " << extract_payload(rmsg) << " from " << extract_senderId(rmsg) << std::endl;

	}

	printf("Done\n");

	return 0;
}

int main(int argc, char **argv) {

	std::cout << "START" << std::endl;


	std::string side(argv[3]);
	if (side == "from_neo")
		return main_cl_rcv(argc, argv);
	else if (side == "to_neo")
		return main_cl(argc, argv);

	printf("Bad usage\n");
	return 0;
}
